import React, {Component} from 'react';
import logo from './logo.svg';

import Form from "react-jsonschema-form";

class MyForm extends Component {

  constructor(props){
    super(props);
    this.state={
       schema :
       {
        "title": "Medical Aid Form",
        "description": "Please fill the form and submit",
        "type": "object",
        "required": [
          "firstName",
          "lastName"
        ],
        "properties": {
          "firstName": {
            "type": "string",
            "title": "First Name",
            "default": "Chuck"
          },
          "lastName": {
            "type": "string",
            "title": "Last name"
          },
          "age": {
            "type": "integer",
            "title": "Age"
          },
          "bio": {
            "type": "string",
            "title": "Bio"
          },
          "password": {
            "type": "string",
            "title": "Password",
            "minLength": 3
          },
          "telephone": {
            "type": "string",
            "title": "Telephone",
            "minLength": 10
          }
        }
      }
     
    
    }
  }
  render(){
    const log = (type) => console.log.bind(console, type);
    const onSubmit = ({formData}, e) => console.log("Data submitted: ",  formData);
    return (
      <div className="App">
      <Form 
        schema={this.state.schema}
        onChange={log("changed")}
        onSubmit={onSubmit}
        onError={log("errors")} />
    </div>
    )
  }
}
export default MyForm;